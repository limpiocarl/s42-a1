const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const showFullName = () => {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  spanFullName.innerHTML = firstName + " " + lastName;
};

txtFirstName.addEventListener("keyup", showFullName);
txtLastName.addEventListener("keyup", showFullName);
